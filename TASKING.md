####2.1 `@PathVariable` 的其他情况
1. should return 200 when bind to int.
2. should return 200 when bind to integer.
2. should return 200 when bind to int and integer.
#### 2.2 `@RequestParam` 及其他情况
1. should return 200 when bind to query string.
2. should return 200 and query when bind to query string.
3. should return 200 and query user default query string.
4. should return 200 when bind collection to param.
#### 2.3 `@RequestBody` 及其其他情况
1. should return date when query date string.
2. should return date when query incorrect date string.
3. should convert object to json.
4. should convert json to object.
#### 2.4 参数验证
1. should valid the param.
2. should valid the param with the size.
3. should valid the param with the min and max.
4. should valid the param with the email.
