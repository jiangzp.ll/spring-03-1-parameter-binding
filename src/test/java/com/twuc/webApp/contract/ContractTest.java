package com.twuc.webApp.contract;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Test;

import java.io.IOException;

import static org.junit.jupiter.api.Assertions.*;

class ContractTest {

    private ObjectMapper objectMapper;

    @Test
    void should_convert_object_to_json() throws JsonProcessingException {
        objectMapper = new ObjectMapper();
        Contract contract = new Contract(1L, "xiaoming");
        String jsonString = objectMapper.writeValueAsString(contract);

        assertEquals("{\"id\":1,\"name\":\"xiaoming\"}", jsonString);
    }
    @Test
    void should_convert_json_to_object() throws IOException {
        objectMapper = new ObjectMapper();
        String jsonString = "{\"id\":1,\"name\":\"xiaoming\"}";
        Contract contract = objectMapper.readValue(jsonString, Contract.class);

        assertEquals("xiaoming", contract.getName());
    }
}
