package com.twuc.webApp.controller;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
class UserControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @Test
    void should_return_200_when_bind_to_int() throws Exception {
        mockMvc.perform(get("/api/users/2"))
                .andExpect(status().isOk())
                .andExpect(content().string("2 is int"));
    }

    @Test
    void should_return_200_when_bind_to_integer() throws Exception {
        mockMvc.perform(get("/api/users/integer/23"))
                .andExpect(status().isOk())
                .andExpect(content().string("23 is integer"));
    }

    @Test
    void should_return_200_when_bind_to_int_and_integer() throws Exception {
        mockMvc.perform(get("/api/users/23/books/2 "))
                .andExpect(status().is(200))
                .andExpect(content().string("user id is 23,book id is 2"));
    }

    @Test
    void should_return_200_when_bind_query_string() throws Exception {
        mockMvc.perform(get("/api/users?username=xiaoming"))
                .andExpect(status().is(200))
                .andExpect(content().string("xiaoming"));
    }

    @Test
    void should_return_200_and_query_when_bind_query_string() throws Exception {
        mockMvc.perform(get("/api/user?username=xiaoming"))
                .andExpect(status().is(200))
                .andExpect(content().string("xiaoming"));
    }

    @Test
    void should_return_200_and_query_user_default_query_string() throws Exception {
        mockMvc.perform(get("/api/users/default"))
                .andExpect(status().is(200))
                .andExpect(content().string("xiaoming"));
    }

    @Test
    void should_bind_collection_to_param() throws Exception {
        List<String> arrayList = new ArrayList<>();
        arrayList.add("value1");
        arrayList.add("value2");
        arrayList.add("value3");
        mockMvc.perform(get("/api/bind/collection?param=value1&param=value2&param=value3"))
                .andExpect(status().is(200));
    }

    @Test
    void should_return_date_when_query_date_string() throws Exception {
        mockMvc.perform(post("/api/datetimes").content("{ \"dateTime\": \"2019-10-01T10:00:00Z\" }")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().is(200))
                .andExpect(content().string("\"2019-10-01\""));
    }

    @Test
    void should_return_date_when_query_incorrect_date_string() throws Exception {
        mockMvc.perform(post("/api/datetimes").content("{ \"dateTime\": \"2019-10-01\" }")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().is(200))
                .andExpect(content().string("\"2019-10-01\""));
    }

    @Test
    void should_valid_the_param() throws Exception {
        mockMvc.perform(post("/api/users/notnull")
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content("{name}"))
                .andExpect(status().is(400));
        mockMvc.perform(post("/api/users/notnull")
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content("{\"name\":\"xiaoming\"}"))
                .andExpect(status().is(200))
                .andExpect(content().string("xiaoming"));
    }

    @Test
    void should_valid_the_param_with_the_size() throws Exception {
        mockMvc.perform(post("/api/size")
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content("{\"name\":\"a\"}"))
                .andExpect(status().is(400));
        mockMvc.perform(post("/api/size")
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content("{\"name\":\"xiaoming\"}"))
                .andExpect(status().is(200))
                .andExpect(content().string("xiaoming"));
    }

    @Test
    void should_valid_the_param_with_the_min_and_max() throws Exception {
        mockMvc.perform(post("/api/minandmax")
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content("{\"name\":\"xiaoming\",\"yearOfBirth\":\"18\"}"))
                .andExpect(status().is(400));
        mockMvc.perform(post("/api/minandmax")
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content("{\"name\":\"xiaoming\",\"yearOfBirth\":\"4000\"}"))
                .andExpect(status().is(200))
                .andExpect(content().string("xiaoming birth is 4000"));
    }

    @Test
    void should_valid_the_param_with_the_email() throws Exception {
        mockMvc.perform(post("/api/email")
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content("{\"name\":\"xiaoming\",\"yearOfBirth\":\"4000\",\"email\":\"123@thoughtworks.com\"}"))
                .andExpect(status().is(200))
                .andExpect(content().string("xiaoming birth is 4000,and his email 123@thoughtworks.com"));
        mockMvc.perform(post("/api/email")
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content("{\"name\":\"xiaoming\",\"yearOfBirth\":\"4000\",\"email\":\"123thoughtworks.com\"}"))
                .andExpect(status().is(400));

    }

    }
