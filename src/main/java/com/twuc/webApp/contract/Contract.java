package com.twuc.webApp.contract;

public class Contract {
    private Long id;
    private String name;

    public Contract() {
    }

    public Contract(Long id, String name) {
        this.id = id;
        this.name = name;
    }

    public Long getId() {
        return id;
    }

    public String getName() {
        return name;
    }
}
