package com.twuc.webApp.controller;

<<<<<<< HEAD
import org.springframework.web.bind.annotation.*;
=======
import com.twuc.webApp.contract.MyDate;
import com.twuc.webApp.contract.User;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.time.LocalDate;
import java.util.ArrayList;
>>>>>>> temp

@RestController
@RequestMapping("/api")
public class UserController {

    @GetMapping("/users/{userId}")
    String getUserWithInt(@PathVariable int userId) {
        return String.format("%d is int", userId);
    }

    @GetMapping("/users/integer/{userId}")
    String getUserWithInteger(@PathVariable Integer userId) {
        return String.format("%d is integer", userId);
    }

    @GetMapping("/users/{userId}/books/{bookId}")
    String getUserAndBook(@PathVariable Integer userId, @PathVariable int bookId) {
        return String.format("user id is %d,book id is %d", userId, bookId);
    }

    @GetMapping("/users")
<<<<<<< HEAD
    String getUser(@RequestParam(defaultValue = "xiaoming") String  userName) {
        return userName;
    }
=======
    String getUserWithParam(@RequestParam String username) {
        return username;
    }

    @GetMapping("/user")
    String getUserWithNotParam( String username) {
        return username;
    }

    @GetMapping("/users/default")
    String getUserWithDefaultParam(@RequestParam(defaultValue = "xiaoming") String username) {
        return username;
    }

    @GetMapping("/bind/collection")
    public ArrayList<String> getCollection(@RequestParam ArrayList<String> param){
        return param;
    }

    @PostMapping("/datetimes")
    public LocalDate getDate(@RequestBody MyDate myDate){
        return myDate.getDateTime();
    }

    @PostMapping("/users/notnull")
    public String getUserName(@RequestBody @Valid User user) {
        return user.getName();
    }

    @PostMapping("/size")
    public String getUserNameWithSize(@RequestBody @Valid User user) {
        return user.getName();
    }

    @PostMapping(value = "/minandmax")
    public String getInfoByNameYear(@RequestBody @Valid User user) {
        return String.format("%s birth is %d", user.getName(), user.getYearOfBirth());
    }

    @PostMapping(value = "/email")
    public String getUserEmail(@RequestBody @Valid User user) {
        return String.format("%s birth is %d,and his email %s", user.getName(), user.getYearOfBirth(), user.getEmail());
    }

>>>>>>> temp
}
